import System.IO
import Data.Char

main = do
  name <- getLine
  writeFile "DeathNote.txt" (map toUpper name)
