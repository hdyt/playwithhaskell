main :: IO ()
main = do
  name <- getLine
  putStrLn ("Hello " ++ name ++ ", Nice to see you here.")