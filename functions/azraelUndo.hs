import System.IO
import System.Directory
import Data.List

main = do
  contents <- readFile "AzraelHandbook.txt"
  let deadPerson = lines contents
      numberingPerson = zipWith (\n line -> show n ++ " - " ++ line) [0..] deadPerson
  putStrLn "These are dead person:"
  mapM_ putStrLn numberingPerson
  putStrLn "Which one do you want to undo?"
  numberString <- getLine
  let number = read numberString
      newDeadPerson = unlines $ delete (deadPerson !! number) deadPerson
  (tempName, tempHandle) <- openTempFile "." "temp"
  hPutStr tempHandle newDeadPerson
  hClose tempHandle
  removeFile "AzraelHandbook.txt"
  renameFile tempName "AzraelHandbook.txt"
