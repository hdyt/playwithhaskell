import System.IO

main = do
  handle <- openFile "docReadFile.txt" ReadMode
  contents <- hGetContents handle
  putStr contents
  hClose handle
