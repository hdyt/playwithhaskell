doubleMe :: Int -> Int
doubleMe x = x + x

doubleUs :: Int -> Int -> Int
doubleUs x y =  doubleMe x + doubleMe y

doubleSmallNumber :: Int -> Int
doubleSmallNumber x
 | x >= 100 = x 
 | x < 100 = doubleMe x 
                           
delNonCaps :: [Char] -> [Char]
delNonCaps text = [ c | c <- text, c `elem` ['A'..'Z']]
