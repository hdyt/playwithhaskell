module Algo where
 listFindSum :: [Int] -> Int -> [(Int,Int)]
 listFindSum list equal = [(a,b) | a <- list, b <- list, a + b == equal] 