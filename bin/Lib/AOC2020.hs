module AOC2020 where
 day1 :: [Int] -> Int -> [(Int,Int)]
 day1 list equal = [(a,b) | a <- list, b <- list, a + b == equal] 