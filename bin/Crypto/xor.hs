import Data.Char(chr, ord)
import Data.Bits(xor)

xor' :: String -> String -> String
xor' text key = map chr $ zipWith xor (map ord text) (map ord $ cycle key)

xor'' :: String -> String -> String
xor'' text key = do
  let mapOrd = \x -> map ord x
  let mapChr = \x -> map chr x
  let zipWithXor = \x -> zipWith xor x
  let textOrd = mapOrd text
  let keyOrd = mapOrd $ cycle key
  mapChr $ zipWithXor textOrd keyOrd
