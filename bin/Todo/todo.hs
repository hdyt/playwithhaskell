import System.Environment
import System.Directory
import System.IO
import Data.List
import Control.Exception

dispatch :: String -> [String] -> IO ()
dispatch "-a" = add
dispatch "-v" = view
dispatch "-d" = remove
dispatch "-h" = help
dispatch "--help" = help
dispatch command = help

main = do
  (command:argList) <- getArgs
  dispatch command argList

add :: [String] -> IO ()
add [fileName, todoItem] = appendFile fileName (todoItem ++ "\n")
add _ = putStrLn "-a takes exactly two arguments, fileName and \"task\", for example, -a todoOnlineTeaching.txt \"Posting a new material\""


view :: [String] -> IO ()
view [fileName] = do
  contents <- readFile fileName
  let todoTasks = lines contents
      numberedTasks = zipWith (\n line -> show n ++ " - " ++ line)
                      [0..] todoTasks
  putStr $ unlines numberedTasks
view _ = putStrLn "-v takes exactly one argument, fileName, for example, -v todoOnlineTeaching.txt"

remove :: [String] -> IO ()
remove [fileName, numberString] = do
  contents <- readFile fileName
  let todoTasks = lines contents
      numberedTasks = zipWith (\n line -> show n ++ " - " ++ line)
                              [0..] todoTasks
  putStrLn "These are your TO-DO items:"
  mapM_ putStrLn numberedTasks
  let number = read numberString
      newTodoItems = unlines $ delete (todoTasks !! number) todoTasks
  bracketOnError (openTempFile "." "temp")
    (\(tempName, tempHandle) -> do
        hClose tempHandle
        removeFile tempName)
    (\(tempName, tempHandle) -> do
        hPutStr tempHandle newTodoItems
        hClose tempHandle
        removeFile fileName
        renameFile tempName fileName)
remove _ = putStrLn "-d takes exactly two argument, fileName and taskNumber, for example, \n0 - Post a new material\n1 - Sleep\n2 - Coding\n-d todoOnlineTeaching.txt 1\nwhich will delete Sleep from the tasks"
    
help :: [String] -> IO ()
help [] = do
  let helps = ["\nAre you lost?\n", "Alfian was here to help you!\n", "Read below help\n", "----------- ******* -----------", "-a fileName \"task\",  to add a task", "-v fileName, to view tasks and it's number", "-d fileName taskNumber, to delete a task", "-h to get help", "----------- ******* -----------"]
  mapM_ putStrLn helps
  

doesntExist :: String -> [String] -> IO ()
doesntExist command _ =
  putStrLn $ "The " ++ command ++ " command doesn't exist"
