mulThree :: Int -> Int
mulThree a = a * 3

mulNine :: Int -> Int
mulNine a = a * (mulThree 3)

-- fp programmer's toolbox map and filter

testMap :: [Int]
testMap = map (+3) [1,2,3]

testLambda :: [Int]
testLambda = map (\x -> x+3) [1,2,3]

filterOneToHundred a = filter a [1..100]
filterOneToHundredLargerThanThree = filter (>3) [1..100]

-- function application ($)

{-
$ is right associative, means it's read read to pass left
while space is left associative f a b means ((f a) b)
while f $ a b is (f (a b))
-}

fapTest = map ($ 9) [(4+), (*4), (+3), sqrt]

{-
compose two or more functions? no problem with (.)
-}

fcompTest = replicate 2 . product . map (*3) $ zipWith max [0,0] [1,1]