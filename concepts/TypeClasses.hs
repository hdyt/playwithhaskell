eq = True == True

ord = 5 > 1

intToStr :: Int -> String
intToStr a = show a

main = do
     if ord
      then putStrLn (intToStr 3)
      else putStrLn (show 4)
