name :: String
name = "Budi"

nim :: Int
nim = 1808096025

list :: [Int]
list = [1,2,3,4,5]

true :: Bool
true = True

tuple :: (String, Int, Bool)
tuple = ("Budi", 1808096025, True) 

main = do
     putStrLn name
     putStrLn (show nim)
     let list2 = map (+2) list
     putStrLn (show list2)
     if true
      then putStrLn "true"
      else putStrLn "false"