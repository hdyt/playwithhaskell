-- pattern matching

laugh :: Int -> String
laugh 1 = "LOL"
laugh 2 = "WKWKWK"
laugh 3 = "GKGKGK"
laugh _ = "HAHAHA"

-- guards

tell :: String -> String
tell a
 | a == "ily" = "I Love You"
 | a == "lol" = "Laugh of Loud"

-- where

ask :: String -> String
ask a
 | tea = person ++ ", a iced tea for you"
 | coffee = "Here, one cup of coffe for " ++ person
 where tea = a == "tea"
       coffee = a == "coffee"
       person = "Budi"
ask "water" = "Cheap"
ask _ = yell
 where yell = "get out!!!"

-- let

fullName :: String
fullName =
    let fstName = "Budi"
        lstName = "man"
    in fstName ++ lstName

-- case

heightCheck :: Int -> String
heightCheck a =
    case a of 170 -> "Hi skinny!"
              160 -> "Hi tall!"
              150 -> "Shorta out of here!"