-- maximum, reverse, repeat, replicate, take, zip, etc.

rec :: [Int] -> Int
rec [] = error "empty"
rec [a] = a
rec (x:xs) = rec xs